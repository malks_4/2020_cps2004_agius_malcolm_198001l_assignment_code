package Applications;

import java.util.ArrayList;
import Users.AccountHolder;
import Accounts.Account;

public class AccountApplication {
    private Account a;
    private ArrayList<AccountHolder> holders;

    public AccountApplication(Account a, ArrayList<AccountHolder> holders){
        this.a = a;
        this.holders = holders;
    }
    
    public Account getAccount(){
        return this.a;
    }

    public ArrayList<AccountHolder> getHolders(){
        return this.holders;
    }
}
