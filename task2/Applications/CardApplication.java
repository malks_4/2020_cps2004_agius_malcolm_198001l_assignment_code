package Applications;

import Accounts.BankCard;
import Accounts.Account;
import Users.AccountHolder;

public class CardApplication {
    private BankCard b;
    private Account a;
    private AccountHolder holder;

    public CardApplication(BankCard b, Account a, AccountHolder holder){
        this.b = b;
        this.a = a;
        this.holder = holder;
    }

    public BankCard getCard(){
        return this.b;
    }

    public Account getAccount(){
        return this.a;
    }

    public AccountHolder getHolder(){
        return this.holder;
    }
}
