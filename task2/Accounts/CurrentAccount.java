package Accounts;

public class CurrentAccount extends Account {

    public CurrentAccount(String currency){
        super(currency);    //calls constructor of parent
    }

    protected boolean isCurrent(){
        return true;
    }
}