package Accounts;

import java.util.ArrayList;
import Users.AccountHolder;

public abstract class Account{
    private static int counter = 0;
    private int accountNumber;
    private ArrayList<AccountHolder> holders = new ArrayList<AccountHolder>();
    private ArrayList<BankCard> cards = new ArrayList<BankCard>();
    private double balance;
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();
    private String currency;

    public Account(String currency){
        this.accountNumber = counter;
        counter++;
        this.balance = 0;
        this.currency = currency;
    }

    protected abstract boolean isCurrent();

    public double getBalance(){
        return this.balance;
    }

    public boolean updateBalance(double amount){
        if(this.balance + amount >=0){
            this.balance += amount;
            return true;
        }
        else{
            return false;
        }
    }

    public String getCurrency(){
        return this.currency;
    }

    public void setCurrency(String currency){
        this.currency = currency;
    }

    public int getAccountNumber(){
        return this.accountNumber;
    }

    public void addTransaction(Transaction t){
        transactions.add(t);
    }

    public void addHolder(AccountHolder holder){
        holders.add(holder);
    }

    public void addCard(BankCard b){
        cards.add(b);
    }

    public void deleteCard(BankCard b){
        cards.remove(b);
    }

    public void deleteCards(){
        this.cards = null;
        //the garbage collector will then delete these cards that have no pointer pointing to them
    }
}