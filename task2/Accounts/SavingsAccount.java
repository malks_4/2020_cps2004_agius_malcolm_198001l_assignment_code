package Accounts;

public class SavingsAccount extends Account {
    final private float interestRate = 0.03f;

    public SavingsAccount(String currency){
        super(currency);    //calls constructor of parent
    }

    protected boolean isCurrent(){
        return false;
    }
}
