package Accounts;

import Users.AccountHolder;

public class BankCard {
    private AccountHolder cardHolder;
    private String cardNumber;
    private Account a;

    public BankCard(String cardNumber){
        this.cardNumber = cardNumber;
    }

    public void setHolder(AccountHolder holder){
        this.cardHolder = holder;
    }

    public Account getAccount(){
        return this.a;
    }

    
}
