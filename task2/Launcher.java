import java.util.ArrayList;
import Users.AccountHolder;
import Users.Administrator;
import Accounts.Account;
import Accounts.SavingsAccount;
import Accounts.CurrentAccount;
import Accounts.BankCard;
import Applications.*;

public class Launcher {
    public static void main(String args[]){
        AccountHolder h1 = new AccountHolder("John","Long","79797979");

        Account a1 = new SavingsAccount("Eur");

        Administrator admin1 = new Administrator("John","Short","21249200");

        ArrayList<AccountHolder> holders1 = new ArrayList<AccountHolder>();

        holders1.add(h1);

        AccountApplication application1 = new AccountApplication(a1,holders1);

        h1.addAccount(application1);    

        try{
            AccountApplication app1 = admin1.getPendingOpenAccount();
            admin1.addAccount(app1);
            System.out.println("Successfully approved account");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("No accounts are pending to be opened");
        }
        
        h1.deposit(a1,500);

        h1.viewBalance(a1);

        h1.withdraw(a1,300);

        h1.viewBalance(a1);

        h1.withdraw(a1,300);

        h1.viewBalance(a1);

        Account a2 = new CurrentAccount("Eur");

        AccountApplication application2 = new AccountApplication(a2,holders1);

        h1.addAccount(application2);

        try{
            AccountApplication app1 = admin1.getPendingOpenAccount();
            admin1.addAccount(app1);
            System.out.println("Success approving account");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("No accounts are pending to be opened");
        }

        h1.moveMoney(a1,a2,100);

        h1.viewBalance(a1);

        h1.viewBalance(a2);

        h1.removeAccount(application1);

        try{
            AccountApplication app1 = admin1.getPendingCloseAccount();
            admin1.removeAccount(app1);
            System.out.println("Success removing Account");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("No accounts are pending to be closed");
        }

        BankCard b1 = new BankCard("13456");

        CardApplication capp = new CardApplication(b1, a2, h1);

        h1.addCard(capp);

        try{
            CardApplication capp1 = admin1.getPendingOpenCard();
            admin1.addCard(capp1);
            System.out.println("Success opening card");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("No cards are pending to be opened");
        }

        h1.removeCard(capp);

        try{
            CardApplication capp1 = admin1.getPendingCloseCard();
            admin1.removeCard(capp1);
            System.out.println("Success closing card");
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("No cards are pending to be closed");
        }

    }
    
}
