package Users;

import java.util.ArrayList;
import Applications.*;
import Accounts.Account;
import Accounts.BankCard;
import Accounts.Transaction;



public class AccountHolder extends RegularUser{
    private ArrayList<Account> accounts = new ArrayList<Account>();
    private ArrayList<AccountApplication> pendingAccounts = new ArrayList<AccountApplication>();
    private ArrayList<BankCard> cards = new ArrayList<BankCard>();
    private ArrayList<CardApplication> pendingCards = new ArrayList<CardApplication>();

    public AccountHolder(String name, String surname, String telephoneNumber){
        super(name, surname, telephoneNumber);
    }

    public void viewBalance(Account a){
        if(this.accounts.contains(a)){
            System.out.println("Your current balance is: "+ a.getCurrency() + a.getBalance());
        }
        else{
            System.out.println("Error: Account specified does not exist/is not yours");
        }
        
    }

    public void moveMoney(Account from, Account to, double amount){
        if(this.accounts.contains(from)){
            boolean check = from.updateBalance(-amount);
            if(check){
                to.updateBalance(amount);
                Transaction t = new Transaction();  //adding transaction to list of transactions
                t.setTo(to);
                t.setFrom(from);
                t.setAmount(amount);
                from.addTransaction(t);
                to.addTransaction(t);
                System.out.println("The amount of "+from.getCurrency()+amount+" have been transferred from account "+from.getAccountNumber()+" to account "+to.getAccountNumber());
            }
            else{
                System.out.println("Error: Insufficient funds to complete this transaction");
            }
        }
        else{
            System.out.println("Error: You cannot transfer money from an account that is not yours");
        }
    }

    public void deposit(Account to, double amount){
        if(this.accounts.contains(to)){
            to.updateBalance(amount);
            Transaction t = new Transaction();
            t.setTo(to);
            t.setFrom(null);
            t.setAmount(amount);
            to.addTransaction(t);
            System.out.println("Successfully deposited "+to.getCurrency()+amount+" to account "+to.getAccountNumber());
        }
        else{
            System.out.println("Error: cannot deposit money to an account that is not yours");
        }
    }

    public void withdraw(Account from, double amount){
        if(this.accounts.contains(from)){
            boolean check = from.updateBalance(-amount);
            if(check){
                Transaction t = new Transaction();
                t.setTo(null);
                t.setFrom(from);
                t.setAmount(amount);
                from.addTransaction(t);
                System.out.println("Successfully withdrawn "+from.getCurrency()+amount+" from account "+from.getAccountNumber());
            }
            else{
                System.out.println("Insufficient funds to complete this withdrawal");
            }
        }
        else{
            System.out.println("Error: cannot withdraw money from an account that is not yours");
        }
    }

    protected void addPendingAccount(AccountApplication a){
        this.pendingAccounts.add(a);
    }

    protected void removePendingAccount(AccountApplication a){
        this.pendingAccounts.remove(a);
    }

    protected void addAccount(Account a){
        this.accounts.add(a);
    }

    protected void removeAccount(Account a){
        this.accounts.remove(a);
    }

    protected void addPendingCard(CardApplication b){
        this.pendingCards.add(b);
    }

    protected void removePendingCard(CardApplication b){
        this.pendingCards.remove(b);
    }

    protected void addCard(BankCard b){
        this.cards.add(b);
    }

    protected void removeCard(BankCard b){
        this.cards.remove(b);
    }
}