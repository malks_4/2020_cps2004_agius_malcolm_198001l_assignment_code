package Users;

import java.util.ArrayList;
import Applications.*;

public class Administrator extends User{

    private static ArrayList<AccountApplication> pendingOpenAccount = new ArrayList<AccountApplication>();    //Lists for pending accounts
    private static ArrayList<AccountApplication> pendingCloseAccount = new ArrayList<AccountApplication>();   //static as these are common for all admins
    private static ArrayList<CardApplication> pendingOpenCard = new ArrayList<CardApplication>();
    private static ArrayList<CardApplication> pendingCloseCard = new ArrayList<CardApplication>();

    public Administrator(String name, String surname, String telephoneNumber){
        super(name, surname, telephoneNumber);
    }

    public static void applyOpenAccount(AccountApplication a){
        pendingOpenAccount.add(a);
        ArrayList<AccountHolder> holders = a.getHolders();
        for(int i = 0;i<holders.size();i++){
            holders.get(i).addPendingAccount(a);
        }
        holders = null;
    }

    public static void applyCloseAccount(AccountApplication a){
        pendingCloseAccount.add(a);
    }

    public static void applyOpenCard(CardApplication b){
        pendingOpenCard.add(b);
        b.getHolder().addPendingCard(b);
    }

    public static void applyCloseCard(CardApplication b){
        pendingCloseCard.add(b);
    }

    public AccountApplication getPendingOpenAccount() throws IndexOutOfBoundsException{     //to be used in main by an admin
        AccountApplication a = pendingOpenAccount.get(0);
        pendingOpenAccount.remove(a);
        return a;
    }
    
    public AccountApplication getPendingCloseAccount() throws IndexOutOfBoundsException{
        AccountApplication a = pendingCloseAccount.get(0);
        pendingCloseAccount.remove(a);
        return a;
    }

    public CardApplication getPendingOpenCard() throws IndexOutOfBoundsException{
        CardApplication b = pendingOpenCard.get(0);
        pendingOpenCard.remove(b);
        return b;
    }

    public CardApplication getPendingCloseCard() throws IndexOutOfBoundsException{  
        CardApplication b = pendingCloseCard.get(0);
        pendingCloseCard.remove(b);
        return b;
    }

    public void addAccount(AccountApplication a){   //abstract methods being implemented
        ArrayList<AccountHolder> holders = a.getHolders();
        for(int i = 0;i<holders.size();i++){
            holders.get(i).addAccount(a.getAccount());
            holders.get(i).removePendingAccount(a);
            a.getAccount().addHolder(holders.get(i));
        }
        holders = null;
        a = null;
    }

    public void removeAccount(AccountApplication a){
        ArrayList<AccountHolder> holders = a.getHolders();
        for(int i = 0;i<holders.size();i++){
            holders.get(i).removeAccount(a.getAccount());
        }
         //must delete all cards associated with this account, and the account itself
        a.getAccount().deleteCards();
        holders = null;
        a = null;
    }

    public void addCard(CardApplication b){
        b.getHolder().addCard(b.getCard());
        b.getHolder().removePendingCard(b);
        b.getAccount().addCard(b.getCard()); //adds Card to the list of cards in this account
        b.getCard().setHolder(b.getHolder());
        b = null;
    }
    
    public void removeCard(CardApplication b){
        b.getHolder().removeCard(b.getCard());
        //must also delete the card itself but not its account 
        b.getAccount().deleteCard(b.getCard()); //deletes card from account list
        b = null;   //deleting application itself
    }
}