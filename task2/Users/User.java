package Users;

import Applications.*;

abstract class User{
    protected String name;
    protected String surname;
    protected String telephoneNumber;

    public User(String name, String surname, String telephoneNumber){
        this.name = name;
        this.surname = surname;
        this.telephoneNumber = telephoneNumber;
    }

    protected abstract void addAccount(AccountApplication a);
    protected abstract void removeAccount(AccountApplication a);
    protected abstract void addCard(CardApplication b);
    protected abstract void removeCard(CardApplication b);
}