package Users;

import Applications.*;

public abstract class RegularUser extends User{

    public RegularUser(String name, String surname, String telephoneNumber){
        super(name, surname, telephoneNumber);
    }

    public void addAccount(AccountApplication a){
        Administrator.applyOpenAccount(a);
    }

    public void removeAccount(AccountApplication a){
        Administrator.applyCloseAccount(a);
    }

    public void addCard(CardApplication b){
        Administrator.applyOpenCard(b);
    }
    
    public void removeCard(CardApplication b){
        Administrator.applyCloseCard(b);
    }
}