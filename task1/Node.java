public class Node{
    private Node nw = null;
    private Node ne = null;
    private Node se = null;
    private Node sw = null;
    private boolean colour; //true = white, false = black

    public void populateQuadtree(boolean image[][]){    //recursively populates quadtree
        
        boolean allSameColour = true;   //checking if the quadrant is all the same colour
        boolean check = image[0][0];    //use first pixel and try to match all the rest
        for(int i = 0;i<image.length;i++){
            for(int j = 0;j<image[i].length;j++){
                if(check != image[i][j]){
                    allSameColour = false;
                    break;
                }
            }
            if(!allSameColour){
                break;
            }
        }

        if(allSameColour){  //base case
            this.colour = check;
        }
        else{   //recursion
            boolean[][] imagenw = new boolean[image.length/2][image[0].length/2];  //making up 4 quadrants
            boolean[][] imagene = new boolean[image.length/2][image[0].length/2];
            boolean[][] imagese = new boolean[image.length/2][image[0].length/2];
            boolean[][] imagesw = new boolean[image.length/2][image[0].length/2];

            for(int i = 0;i<image.length/2;i++){    //half the pixels go into nw and ne
                for(int j = 0;j<image[i].length/2;j++){ //half the pixels in this line go to nw
                    imagenw[i][j] = image[i][j];
                }
                for(int j = image[i].length/2;j<image[i].length;j++){ //the other half go to ne
                    imagene[i][j-image[i].length/2] = image[i][j];
                }
            }

            for(int i = image.length/2;i<image.length;i++){ //half the pixels go into sw and se
                for(int j = 0;j<image[i].length/2;j++){ //half the pixels in this line go to sw
                    imagesw[i-image.length/2][j] = image[i][j];
                }
                for(int j = image[i].length/2;j<image[i].length;j++){ //the other half go to se
                    imagese[i-image.length/2][j-image[i].length/2] = image[i][j];
                }
            }
            this.nw = new Node();    //allocating memory for children
            this.ne = new Node();
            this.se = new Node();
            this.sw = new Node();

            this.nw.populateQuadtree(imagenw);
            this.ne.populateQuadtree(imagene);
            this.se.populateQuadtree(imagese);
            this.sw.populateQuadtree(imagesw);   //recursing to populate quadtree
        }
    
    }

    public void testQuadtree(){
        if(this.nw == null){    //this checks if quadrant is grey or not
            if(this.colour){
                System.out.println("This quadrant is white");
            }
            else{
                System.out.println("This quadrant is black");
            }
        }
        else{
            nw.testQuadtree();
            ne.testQuadtree();
            se.testQuadtree();
            sw.testQuadtree();
        }
    }
    
}