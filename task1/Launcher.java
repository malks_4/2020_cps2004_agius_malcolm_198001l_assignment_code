import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Launcher {
    public static void main(String args[]) throws FileNotFoundException {
        File imagefile = new File(args[0]);  //getting size of image from size of file

        if(args[0].endsWith(".txt")){   //different parsing according to file type
            long size = imagefile.length();
            double sqrtSize = Math.sqrt((double)size);
            boolean[][] image = new boolean[(int)Math.floor(sqrtSize)][(int)Math.floor(sqrtSize)];  //image in the form of true or false
        
            Scanner scnr = new Scanner(imagefile);
            int lineCount = 0;
            while(scnr.hasNextLine()){      //save image as true or false (white or black pixel)
                String line = scnr.nextLine();
                for(int i = 0;i<line.length();i++){
                    image[lineCount][i] = (line.charAt(i) == 'T');
                }
                lineCount++;
            }
            scnr.close();   //closing scanner
            //parse text file  //takes text file as command line argument

            Node root = new Node();
            root.populateQuadtree(image);   //populate quadtree
            root.testQuadtree();   //this method outputs 11 white quadrants and 8 black quadrants which match my calculations
        }
        else if(args[0].endsWith(".csv")){  //if csv file different parsing
            Scanner scnr = new Scanner(imagefile);
            int maxPoint = 0;
            ArrayList<String[]> points = new ArrayList<String[]>(); //hold all points as strings
            while(scnr.hasNextLine()){
                String[] point = scnr.nextLine().split(",",0);
                points.add(point);
                if(Integer.parseInt(point[0])>maxPoint){
                    maxPoint = Integer.parseInt(point[0]);
                }
                if(Integer.parseInt(point[1])>maxPoint){
                    maxPoint = Integer.parseInt(point[1]);
                }
            }
            scnr.close();
            int imageSize = 1;
            
            while(imageSize<maxPoint){  //calculating imageSize (smallest possible image)
                imageSize*=2;
            }
            boolean[][] image = new boolean[imageSize][imageSize];
      
            for(String[] point : points){
                image[Integer.parseInt(point[0])-1][Integer.parseInt(point[1])-1] = true;
            }  //populates image array

            Node root = new Node();
            root.populateQuadtree(image);   //populate quadtree
            root.testQuadtree();   //this method outputs 11 white quadrants and 8 black quadrants which match my calculations
        }

    }
    
}
