#include "myuint.h"
#include <iostream>
#include <limits>

int main(){
    myuint<8> a(10);
    myuint<8> b(8);
    myuint<8> x(32);

    myuint<8> z;

    cout << x.getBinary() << endl;
    cout << x.template convert_to<int>() << endl;

    return 0;
}
