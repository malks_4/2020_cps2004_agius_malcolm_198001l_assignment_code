#include <vector>
#include <cmath>
#include <string>
#include <iostream>
using namespace std;

template <int bits>
class myuint{
    static_assert(floor(log2(bits)) == log2(bits), "Only exponents of 2 are supported as number of bits by myuint"); //raises a compile-time error if bits is not in the form of 2^n
    static_assert(bits <= 2048, "The maximum number of bits supported by myuint is 2048"); //raises a compile-time error if the number of bits is greater than 2018
    static_assert(bits > 0, "The number of bits cannot be 0 or less in myuint");

    private:
        vector<char> binary; //the number in binary
        void fill(); //fills in all the bits that the user specified through template
        //auto decimal;
        
    
    public:
        myuint(unsigned long long num); //constructor
        myuint(); //empty constructor used for initialisation
        template<typename T>
        T convert_to(); //template function that converts
        myuint<bits> operator+(myuint other); //binary addition
        myuint<bits> operator+=(myuint other); //+= operator
        myuint<bits> operator++(); //increment
        myuint<bits> operator++(int); //increment
        myuint<bits> operator-(myuint other); //binary subtraction
        myuint<bits> operator-=(myuint other); //-= operator
        myuint<bits> operator--(); //decrement
        myuint<bits> operator--(int); //decrement
        myuint<bits> operator<<(myuint other); //binary shift left
        myuint<bits> operator<<=(myuint other); //<<= operator
        myuint<bits> operator>>(myuint other); //binary shift right
        myuint<bits> operator>>=(myuint other); //>>= operator
        bool operator>(myuint other); //greater than
        bool operator>=(myuint other); //greater than or equal to
        bool operator<(myuint other); //less than
        bool operator<=(myuint other); //less than or equal to
        bool operator==(myuint other); //equal to
        bool operator!=(myuint other); //not equal to
        myuint<bits> operator*(myuint other); //binary multiplication
        myuint<bits> operator*=(myuint other); //*= operator
        myuint<bits> operator/(myuint other); //binary division
        myuint<bits> operator/=(myuint other); ///= operator
        myuint<bits> operator%(myuint other); //binary modulus
        myuint<bits> operator%=(myuint other); //%= operator
        string getBinary(); //returns the binary value as a string for testing purposes
        
};

template<int bits>
string myuint<bits>::getBinary(){ //turns vector into string for testing purposes
    string ans = "";
    for(int i = 0; i<(int)this->binary.size(); i++){
        ans = ans + this->binary[i];
    }
    return ans;
}

template<int bits>
void myuint<bits>::fill(){ //fills in all the bits that the user specified through template
    while(this->binary.size()<bits){
        this->binary.insert(this->binary.begin(),'0'); 
    }
}

template <int bits>
myuint<bits>::myuint(unsigned long long num){ //the constructor converts the number to binary
    vector<char> bin; //temporary binary number
    while(num>0){
        if(num%2 == 1){ //converting to binary
            bin.insert(bin.begin(),'1');
        }
        else{
            bin.insert(bin.begin(),'0');
        }
        num = floor(num/2);
    }
    this->binary = bin;
    this->fill();
}

template <int bits>
myuint<bits>::myuint(){ //constructor for empty myuint

}

template <int bits>
template<typename T>
 T myuint<bits>::convert_to(){ //template function that converts to specified 
    T ans = 0;
    int size = sizeof(T)*8;

    vector<char> bin; 

    if(size>bits){
        bin = this->binary;
    }
    else{
        bin = vector<char>(this->binary.end()-size,this->binary.end()); //slice LSB of vector into subvector
    }

    int index = bin.size()-1;
    for(int i = 0;i<(int)bin.size();i++){
        if(bin[i] == '1'){
            ans += pow(2,index);
        }
        index--;
    }

    return ans;
 } 

template <int bits>
myuint<bits> myuint<bits>::operator+(myuint other){ //binary addition
    myuint<bits> ans;

    int sum = 0; //sum of digits
    int i = this->binary.size() -1;
    int j = other.binary.size() -1;

    while(sum==1 || i>=0 || j>=0){
        sum += ((i >= 0)? this->binary[i] - '0': 0); 
        sum += ((j >= 0)? other.binary[j] - '0': 0); 
  
        ans.binary.insert(ans.binary.begin(),char(sum % 2 + '0')); // 1+0=1, 1+1=0, 1+1+1=1
  
        sum /= 2; //to get carry
  
        i--; 
        j--; 
    }
    ans.fill();
    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator+=(myuint other){ //+= operator
    *this = *this + other;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator++(){ //increment prefix
    *this = *this+1;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator++(int){ //increment postfix
    myuint<bits> copy = *this;
    *this = *this+1;
    return copy;
}

template <int bits>
myuint<bits> myuint<bits>::operator-(myuint other){ //performs binary subtraction by using addition
    myuint<bits> ans;
    for(int i = 0;i<(int)other.binary.size();i++){ //flips all binary characters
        if(other.binary[i] == '1'){
            other.binary[i] = '0';
        }
        else{
            other.binary[i] = '1';
        }
    }
    other.fill();
    other = other + 1; //adds 1 to binary
    ans = *this + other; //performs (subtraction)

    if(ans.binary.size()>bits)
    ans.binary.erase(ans.binary.begin());

    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator-=(myuint other){ //-= operator
    *this = *this - other;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator--(){ //decrement prefix
    *this = *this-1;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator--(int){ //decrement postfix
    myuint<bits> copy = *this;
    *this = *this-1;
    return copy;
}

template <int bits>
myuint<bits> myuint<bits>::operator<<(myuint other){ //binary shift left
    myuint<bits> ans = *this;
    for(myuint<bits> i(0);i<other;i++){
        ans.binary.erase(ans.binary.begin());
        ans.binary.insert(ans.binary.end(),'0');
    }
    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator<<=(myuint other){ //<<= operator
    *this = *this << other;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator>>(myuint other){ //binary shift right
    myuint<bits> ans = *this;
    for(myuint<bits> i(0);i<other;i++){
        ans.binary.erase(ans.binary.end()-1);
        ans.binary.insert(ans.binary.begin(),'0');
    }
    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator>>=(myuint other){ //<<= operator
    *this = *this >> other;
    return *this;
}

template <int bits>
bool myuint<bits>::operator>(myuint other){ //greater than
    bool ans = false;
    for(int i = 0;i<bits;i++){
        if(this->binary[i]!=other.binary[i]){
            if(this->binary[i]=='1'){
                ans = true;
                break;
            }
            else{
                break;
            }
        }
    }
    return ans;
}

template <int bits>
bool myuint<bits>::operator<(myuint other){ //less than
    bool ans = false;
    for(int i = 0;i<bits;i++){
        if(this->binary[i]!=other.binary[i]){
            if(this->binary[i]=='1'){
                break;
            }
            else{
                ans = true;
                break;
            }
        }
    }
    return ans;
}

template <int bits>
bool myuint<bits>::operator>=(myuint other){ //greater than or equal to
    return !(*this < other);
}

template <int bits>
bool myuint<bits>::operator<=(myuint other){ //less than or equal to
    return !(*this > other);
}

template <int bits>
bool myuint<bits>::operator==(myuint other){ //equal to (condition)
    bool ans = true;
    for(int i = 0;i<bits;i++){
        if(this->binary[i]!=other.binary[i]){
            ans = false;
            break;
        }
    }
    return ans;
}

template <int bits>
bool myuint<bits>::operator!=(myuint other){ //not equal to (condition)
    return !(*this == other);
}

template <int bits>
myuint<bits> myuint<bits>::operator*(myuint other){ //binary multiplacation (loop of addition)
    int s = 0; //shifter
    myuint<bits> ans;
    myuint<bits> temp;
    int i = other.binary.size()-1;
    while(i>=0){
        if(other.binary[i]=='1'){
            temp.binary = this->binary;
            for(int j = 0;j<s;j++){
                temp.binary.insert(temp.binary.end(),'0');
            }
            ans += temp;
        }
        s++;
        i--;
    }
    while(ans.binary.size()>bits){ //to size the number appropriately
        ans.binary.erase(ans.binary.begin());
    }
    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator*=(myuint other){ //*= operator
    *this = *this * other;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator/(myuint other){ //binary division
    //dividend = *this, divisor = other
    /*myuint<bits> quotient(0);
    myuint<bits> temp(0);
    myuint<bits> remainder;
    
    for(int i = 0;i<bits;i++){
        if(other>temp){
            temp.binary.erase(temp.binary.begin());
            temp.binary.insert(temp.binary.end(),this->binary[i]);
            quotient.binary.erase(quotient.binary.begin());
            quotient.binary.insert(quotient.binary.end()-1,'0');
        }
        else{
            quotient.binary.erase(quotient.binary.begin());
            quotient.binary.insert(quotient.binary.end()-1,'1');
            temp-=other;
        }
    }
    
    return quotient;*/

    myuint<bits> ans(0);
    myuint<bits> iterator(0);

    while((iterator+other)<=*this){
        iterator+=other;
        ans++;
    }
    return ans;
}

template <int bits>
myuint<bits> myuint<bits>::operator/=(myuint other){ ///= operator
    *this = *this / other;
    return *this;
}

template <int bits>
myuint<bits> myuint<bits>::operator%(myuint other){ //binary modulus
    /*myuint<bits> quotient(0);
    myuint<bits> temp(0);
    myuint<bits> remainder;
    
    for(int i = 0; i<bits;i++){
        if(other>temp){
            temp.binary.erase(temp.binary.begin());
            temp.binary.insert(temp.binary.end(),this->binary[i]);
            quotient.binary.erase(quotient.binary.begin());
            quotient.binary.insert(quotient.binary.end()-1,'0');
        }
        else{
            quotient.binary.erase(quotient.binary.begin());
            quotient.binary.insert(quotient.binary.end()-1,'1');
            temp-=other;
        }
    }
    

    return temp;*/


    myuint<bits> ans(0);
    myuint<bits> iterator(0);

    while((iterator+other)<=*this){
        iterator+=other;
        ans++;
    }
    return *this - iterator;
}

template <int bits>
myuint<bits> myuint<bits>::operator%=(myuint other){ //%= operator
    *this = *this % other;
    return *this;
}

